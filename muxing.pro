QT -= gui

LIBS += -L$(FFMPEGDIR)/lib
LIBS += -lavcodec -lavformat -lswscale -lavutil -lavfilter -lswresample

win32-msvc2003|win32-msvc2005|win32-msvc2008|win32-msvc2010|win32-msvc2012|win32-msvc2013: QMAKE_LFLAGS_RELEASE += /OPT:NOREF

INCLUDEPATH += $(FFMPEGDIR)/include
DEPENDPATH  += $(FFMPEGDIR)/include

TARGET = muxing
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += muxing.c
